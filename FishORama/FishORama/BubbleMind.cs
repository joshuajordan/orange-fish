﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;              // Required to use XNA features.
using XNAMachinationisRatio;                // Required to use the XNA Machinationis Ratio Engine general features.
using XNAMachinationisRatio.AI;             // Required to use the XNA Machinationis Ratio general AI features.


/* LERNING PILL: XNAMachinationisRatio Engine
 * XNAMachinationisRatio is an engine that allows implementing
 * simulations and games based on XNA, simplifying the use of XNA
 * and adding features not directly available in XNA.
 * XNAMachinationisRatio is a work in progress.
 * The engine works "under the hood", taking care of many features
 * of an interactive simulation automatically, thus minimizing
 * the amount of code that developers have to write.
 * 
 * In order to use the engine, the application main class (Kernel, in the
 * case of FishO'Rama) creates, initializes and stores
 * an instance of class Engine in one of its data members.
 * 
 * The classes comprised in the  XNA Machinationis Ratio engine and the
 * related functionalities can be accessed from any of your XNA project
 * source code files by adding appropriate 'using' statements at the beginning of
 * the file. 
 * 
 */

namespace FishORama
{

    class BubbleMind : AIPlayer
    {
        #region Data Members
        private float mFacingDirection;         // Direction the fish is facing (1: right; -1: left).


        private AquariumToken mAquarium;    // Reference to the aquarium in which the creature lives.
        private BubbleToken mToken;       // Reference to the aquarium in which the creature lives.
        private OrangeFishToken mOrangeFish; //Reference to the orangefish object
        private chestToken mChest; //Reference to the chest object

       
        private int mSpeed = 3;
        private static Random random = new Random(); //generator for all objects
        #endregion

        #region Properties

        /// <summary>
        /// Set Aquarium in which the mind's behavior should be enacted.
        /// </summary>
        public AquariumToken Aquarium
        {
            set { mAquarium = value; }
        }

        public OrangeFishToken OrangeFish //access to token
        {
            set { mOrangeFish = value; }
        }

        public chestToken Chest //access to token
        {
            set { mChest = value; }
        }

        /// <summary>
        /// Get/Set initial position on Y axis.
        /// </summary>


        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="pToken">Token to be associated with the mind.</param>
        public BubbleMind(X2DToken pToken)
        {
            this.Possess(pToken);       // Possess token.
            mFacingDirection = 1;

            this.mSpeed = random.Next(3, 5); //random speed for objects
        }



        #endregion

        #region Methods
    
        /// <summary>
        /// AI Update method.
        /// </summary>
        /// <param name="pGameTime">Game time</param>
        public override void Update(ref GameTime pGameTime)
        {
            Vector3 tokenPosition = this.PossessedToken.Position;

            tokenPosition.Y += mSpeed;
            tokenPosition.X += (float)Math.Sin(tokenPosition.Y / 42);  //sine of specified angle
            this.PossessedToken.Position = tokenPosition;

           
                if (tokenPosition.Y >= mOrangeFish.Position.Y + 150) //reset greater than 150 -> then
                {
                    this.PossessedToken.Position = mOrangeFish.Position; //place on the fish location                  
                }
            
           

                if(this.PossessedToken.Name == "chestBubble1")
                {
                    if(this.PossessedToken.Position.Y >= mChest.Position.Y + 300) //reset greater than 300 -> then
                    {
                        this.PossessedToken.Position = mChest.Position;  //place at the chest location
                        mSpeed = random.Next(1, 7);
                    }           
                }


                if (this.PossessedToken.Name == "chestBubble2")
                {
                    if (this.PossessedToken.Position.Y >= mChest.Position.Y + 300)
                    {
                        this.PossessedToken.Position = mChest.Position;
                        mSpeed = random.Next(5, 10);                   
                    }
                }

                this.PossessedToken.Orientation = new Vector3(mFacingDirection,
                                                                  this.PossessedToken.Orientation.X,
                                                                  this.PossessedToken.Orientation.Z);    
        #endregion
        }
    }

}


