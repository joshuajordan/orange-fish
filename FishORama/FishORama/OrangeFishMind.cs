﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;              // Required to use XNA features.
using XNAMachinationisRatio;                // Required to use the XNA Machinationis Ratio Engine general features.
using XNAMachinationisRatio.AI;             // Required to use the XNA Machinationis Ratio general AI features.


/* LERNING PILL: XNAMachinationisRatio Engine
 * XNAMachinationisRatio is an engine that allows implementing
 * simulations and games based on XNA, simplifying the use of XNA
 * and adding features not directly available in XNA.
 * XNAMachinationisRatio is a work in progress.
 * The engine works "under the hood", taking care of many features
 * of an interactive simulation automatically, thus minimizing
 * the amount of code that developers have to write.
 * 
 * In order to use the engine, the application main class (Kernel, in the
 * case of FishO'Rama) creates, initializes and stores
 * an instance of class Engine in one of its data members.
 * 
 * The classes comprised in the  XNA Machinationis Ratio engine and the
 * related functionalities can be accessed from any of your XNA project
 * source code files by adding appropriate 'using' statements at the beginning of
 * the file. 
 * 
 */

namespace FishORama
{
    /* LEARNING PILL: Token behaviors in the XNA Machinationis Ratio engine
     * Some simulation tokens may need to enact specific behaviors in order to
     * participate in the simulation. The XNA Machinationis Ratio engine
     * allows a token to enact a behavior by associating an artificial intelligence
     * mind to it. Mind objects are created from subclasses of the class AIPlayer
     * included in the engine. In order to associate a mind to a token, a new
     * mind object must be created, passing to the constructor of the mind a reference
     * of the object that must be associated with the mind. This must be done in
     * the DefaultProperties method of the token.
     * 
     * Hence, every time a new tipe of AI mind is required, a new class derived from
     * AIPlayer must be created, and an instance of it must be associated to the
     * token classes that need it.
     * 
     * Mind objects enact behaviors through the method Update (see below for further details). 
     */
    class OrangeFishMind : AIPlayer
    {
       
        #region Data Members
        private AquariumToken mAquarium;    // Reference to the aquarium in which the creature lives.

        /* ---------------- BOOLEANS DATA MEMBERS------------------ */
        private bool doBehaviour = true;
        private bool pickBehaviour = false;
        private bool acceleration = false;
        private bool acceleration1 = false;
        private bool timeSet = false;

        /* ---------------- FLOATS DATA MEMBERS ------------------ */
        private float mFacingDirection;         // Direction the fish is facing (1: right; -1: left).
        private float yFacingDirection;
        private float mSpeed = 3;
        private float sinking;
        private float origionalPosition;
        private float sinkDistance;
        private float  distanceTraveled = 0;
        private float accelerate = 0; //for accelleration behavour
        private float beginSpeed;


        /* ---------------- DOBULES DATA MEMBERS------------------ */
        private double currentTime;
        private double timeFinsihed;

        /* ---------------- INTS DATA MEMBERS ------------------ */
        private int accelTime = 900; //time defined in miliseconds
        private int pick;

        /* ---------------- RANDOM GENERATOR FOR SWITCH CASE ------------------ */
        private Random randomBehaviourGenerator = new Random();
        
        #endregion

        #region Properties

        /// <summary>
        /// Set Aquarium in which the mind's behavior should be enacted.
        /// </summary>
        public AquariumToken Aquarium
        {
            set { mAquarium = value; }
        }
        
        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="pToken">Token to be associated with the mind.</param>
        public OrangeFishMind(X2DToken pToken) 
        {
            /* LEARNING PILL: associating a mind with a token
             * In order for a mind to control a token, it must be associated with the token.
             * This is done when the mind is constructed, using the method Possess inherited
             * from class AIPlayer.
             */
           mFacingDirection = -1;
           yFacingDirection = -1;
           this.Possess(pToken);

        } 

        #endregion

        #region Methods

        /* LEARNING PILL: The AI update method.
         * Mind objects enact behaviors through the method Update. This method is
         * automatically invoked by the engine, periodically, 'under the hood'. This can be
         * be better understood that the engine asks to all the available AI-based tokens:
         * "Would you like to do anything at all?" And this 'asking' is done through invoking
         * the Update method of each mind available in the system. The response is the execution
         * of the Update method of each mind , and all the methods possibly triggered by Update.
         * 
         * Although the Update method could invoke other methods if needed, EVERY
         * BEHAVIOR STARTS from Update. If a behavior is not directly coded in Updated, or in
         * a method invoked by Update, then it is IGNORED.
         * 
         */

        /// <summary>
        /// AI Update method.
        /// </summary>
        /// <param name="pGameTime">Game time</param>
        /// 
        /* -------------PLEASE READ. -> I AM USING BOTH GAMETIME AND USING THE FRAMES PER SECOND TO WORK OUT THE TIMER. I understand that I could have just used gametime
         * but I was having problems with it on some behaviours, that's why I switched... it works though  */


        private Vector3 HorizontalSwimBehaviour(Vector3 tokenPosition)
        {
            tokenPosition.X = tokenPosition.X + mSpeed * mFacingDirection;

            distanceTraveled = distanceTraveled + mSpeed; //increased the distance traveled->for behaviours

            if (tokenPosition.X > 350 || tokenPosition.X < -350) 
            {
                mFacingDirection = -mFacingDirection; 
            }
 
            return tokenPosition; 
        }

        private Vector3 VerticalSwimBehaviour(Vector3 tokenPosition)
        {
            tokenPosition.Y = tokenPosition.Y + mSpeed * yFacingDirection;

            this.PossessedToken.Position = tokenPosition;

            if (tokenPosition.Y > 300 || tokenPosition.Y < -300)
            {
               // yFacingDirection = -yFacingDirection;
            }

            return tokenPosition;
        }

        /* ------------------ DASHING BEHAVIOUR ------------------*/

        private Vector3 DashBehaviour(Vector3 tokenPosition)
        {
            if (doBehaviour) //if true
            {
                mSpeed += 10;
                distanceTraveled = 0; //reset 0
                doBehaviour = false; //break out of iteration
               // Console.WriteLine("Speed increased: " + mSpeed);
            }
              distanceTraveled += mSpeed; //increase distance...
            
            if (distanceTraveled >= 250)
            {
                mSpeed -= 10; 
                distanceTraveled = 0; //reset distance traveled
                doBehaviour = true; //restart iteration
                pickBehaviour = false; //break out of case iteration
            }

            return tokenPosition;
        }

        /* ------------------ ACCELERATION BEHAVIOUR ------------------*/
        private void AccelerationBehaviour ()
        {
            if (!acceleration) //if false
            {
                accelTime = 900; 
                accelerate = 0.5f;  //increase acceleration
                beginSpeed = mSpeed;
                acceleration = true; //break out of iteration  
            }
            
            //set time for deceleration
            if (acceleration) //if speed reaches tweleve then->
            {
                if (!acceleration1)
                {
                     mSpeed = mSpeed + accelerate; // increase....   
 
                }
               
                if(mSpeed == beginSpeed + 12)
                {
                    accelTime -= 1;
                    acceleration1 = true;
                                   
                }
                if (accelTime == 0)  //if current time is greater than 15 seconds ->  then
                {
                    mSpeed -= accelerate; // start decleration
                   // Console.WriteLine(accelTime);
                }
                if (mSpeed == beginSpeed)
                {
                    accelTime = 900; 
                    mSpeed = 2;
                    acceleration = false; //break out of iteration
                    acceleration1 = false; //break out of iteration
                    pickBehaviour = false; //break out of case iteration
                }
            }                        
        }

        /* ------------------ HUNGRY BEHAVIOUR ------------------*/
        private Vector3 HungryBehvaiour(Vector3 tokenPosition)
        {
            if (doBehaviour) //if true
            {
                distanceTraveled = 0; //reset
                mSpeed = 1;
                accelTime = 300;
                doBehaviour = false; //break out of iteration
            }

            if (distanceTraveled >= 150) //swim back forth 150px
            {
                distanceTraveled = 0; //reset
                mFacingDirection = -mFacingDirection;
            }

            if(tokenPosition.Y <= 250) //less than 250 then->
            {
                yFacingDirection = 1; //right
                tokenPosition = VerticalSwimBehaviour(tokenPosition);  //swim upwards          
            }
            accelTime -= 1; //decrease timer
            if(accelTime == 1) 
            {
                distanceTraveled = 0; //reset
                doBehaviour = true; //break out of iteration 
                pickBehaviour = false; //break out of case iteration
            }
            
            return tokenPosition;
           
        }

        /* ------------------ SINK BEHAVIOUR ------------------*/

        private Vector3 SinkBehaviour(Vector3 tokenPosition)
        {
            if(doBehaviour) //if ture->do this
            {
                sinking = randomBehaviourGenerator.Next(50, 150); //sink between 50 - 150

                origionalPosition = tokenPosition.Y; //get origional position of object
                sinkDistance = origionalPosition - sinking; //sink down on the y axes (depth depends on random value)
                yFacingDirection = -1; //left

                distanceTraveled = 0; //reset
                doBehaviour = false; //break out of iteration
            }

            if(sinkDistance < -200) //distance less than
            {
                sinkDistance = -200; //decrease
            }

            if(tokenPosition.Y > sinkDistance) 
            {
                yFacingDirection = -1; 
                tokenPosition = VerticalSwimBehaviour(tokenPosition); //swim downwards on y axes
                
            }
            else
            {
                doBehaviour = true; //break out of iteration
                pickBehaviour = false; //break out of case iteration
            }
            return tokenPosition;
        }


        public override void Update(ref GameTime pGameTime)
        {
            currentTime = pGameTime.TotalGameTime.TotalSeconds;

            Vector3 tokenPosition = this.PossessedToken.Position;

          tokenPosition = HorizontalSwimBehaviour(tokenPosition); 

            if (!pickBehaviour) //if not true-> then 
            {
                pick = randomBehaviourGenerator.Next(1, 1000); //pick number        
            }

            switch (pick) //numbers
            {
                case 678:
                    {
                        pickBehaviour = true; //break out of random generator conditional statement
                        tokenPosition = HungryBehvaiour(tokenPosition);
                    }
                    break;
                case 478:
                    {
                        pickBehaviour = true; 
                        tokenPosition = SinkBehaviour(tokenPosition);
                    }
                    break;

                case 910:
                    {
                        pickBehaviour = true;
                        tokenPosition = DashBehaviour(tokenPosition);                        
                    }
                    break;
                case 339:
                    {
                        pickBehaviour = true;
                        tokenPosition = HorizontalSwimBehaviour(tokenPosition);
                        AccelerationBehaviour();       
                   }
                    break;

                default:
                    tokenPosition = HorizontalSwimBehaviour(tokenPosition);
                    break;
            }

            this.PossessedToken.Orientation = new Vector3(mFacingDirection, this.PossessedToken.Orientation.Y, this.PossessedToken.Orientation.Z);

           

          this.PossessedToken.Position = tokenPosition;

        }

        #endregion
    }
}
