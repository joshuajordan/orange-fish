﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;              // Required to use XNA features.
using XNAMachinationisRatio;                // Required to use the XNA Machinationis Ratio Engine general features.
using XNAMachinationisRatio.AI;             // Required to use the XNA Machinationis Ratio general AI features.
using System.Diagnostics;
using System.Threading;

/* LERNING PILL: XNAMachinationisRatio Engine
 * XNAMachinationisRatio is an engine that allows implementing
 * simulations and games based on XNA, simplifying the use of XNA
 * and adding features not directly available in XNA.
 * XNAMachinationisRatio is a work in progress.
 * The engine works "under the hood", taking care of many features
 * of an interactive simulation automatically, thus minimizing
 * the amount of code that developers have to write.
 * 
 * In order to use the engine, the application main class (Kernel, in the
 * case of FishO'Rama) creates, initializes and stores
 * an instance of class Engine in one of its data members.
 * 
 * The classes comprised in the  XNA Machinationis Ratio engine and the
 * related functionalities can be accessed from any of your XNA project
 * source code files by adding appropriate 'using' statements at the beginning of
 * the file. 
 * 
 */

namespace FishORama
{
    /* LEARNING PILL: Token behaviors in the XNA Machinationis Ratio engine
     * Some simulation tokens may need to enact specific behaviors in order to
     * participate in the simulation. The XNA Machinationis Ratio engine
     * allows a token to enact a behavior by associating an artificial intelligence
     * mind to it. Mind objects are created from subclasses of the class AIPlayer
     * included in the engine. In order to associate a mind to a token, a new
     * mind object must be created, passing to the constructor of the mind a reference
     * of the object that must be associated with the mind. This must be done in
     * the DefaultProperties method of the token.
     * 
     * Hence, every time a new tipe of AI mind is required, a new class derived from
     * AIPlayer must be created, and an instance of it must be associated to the
     * token classes that need it.
     * 
     * Mind objects enact behaviors through the method Update (see below for further details). 
     */
    class PiranhaMind : AIPlayer
    {

        #region Data Members

        private AquariumToken mAquarium;        // Reference to the aquarium in which the creature lives.

    /* --------------- BOOLEANS -------------------*/
        private bool feed = false;
        private bool patrol = false;
        private bool timeset = false;
        private bool startingPos = false;

        /* --------------- FLOATS -------------------*/
        private float mFacingDirection;         // Direction the fish is facing (1: right; -1: left).
        private float mSpeed = 3;

        /* --------------- VECTORS -------------------*/

        private Vector3 currentPosition;

        /* --------------- DOUBLES -------------------*/
        private double currentTime;
        private double timeFinished;

        /* --------------- RANDOM CLASS -------------------*/
        Random random = new Random();
      
   

        #endregion

        #region Properties

        /// <summary>
        /// Set Aquarium in which the mind's behavior should be enacted.
        /// </summary>
        public AquariumToken Aquarium
        {
            set { mAquarium = value; }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="pToken">Token to be associated with the mind.</param>
        public PiranhaMind(X2DToken pToken)
        {
            /* LEARNING PILL: associating a mind with a token
             * In order for a mind to control a token, it must be associated with the token.
             * This is done when the mind is constructed, using the method Possess inherited
             * from class AIPlayer.
             */
            mFacingDirection = 1;
            this.Possess(pToken);
        }

        #endregion

        #region Methods

        /// <summary>
        /// AI Update method.
        /// </summary>
        /// <param name="pGameTime">Game time</param>
      #region pHungry
        private Vector3 PHungry(Vector3 tokenPosition)
        {
            tokenPosition.X = tokenPosition.X + mSpeed * mFacingDirection;

            if (tokenPosition.X > 350 || tokenPosition.X < -350) 
            {
                mFacingDirection = -mFacingDirection;
                mSpeed = 8;
            }

            return tokenPosition;
        }
       #endregion


   /* ------------------ FULL STATE ----------------------- */

       #region fullState

        private Vector3 fullState(Vector3 tokenPosition)
        {
            if (patrol) //rue
            {
                if (tokenPosition.X < currentPosition.X) //less than->then
                {
                    tokenPosition.X = tokenPosition.X + mSpeed; //move
                    mFacingDirection = -1; //left

                }
                else
                {
                    tokenPosition.X = tokenPosition.X - mSpeed;//move
                    mFacingDirection = 1; //right
                }

                if (tokenPosition.Y < currentPosition.Y) //less than->then
                {
                    tokenPosition.Y = tokenPosition.Y + mSpeed; //move

                }
                else
                {
                    tokenPosition.Y = tokenPosition.Y - mSpeed; //move
                }
                if(Math.Abs(tokenPosition.Y - currentPosition.Y) < 5 ) //position less than 5-> then
	            {
                    tokenPosition.X = tokenPosition.X + mSpeed * mFacingDirection; //move away/towards position
	            }
            }

            if (!timeset) // if false
            {
                timeFinished = currentTime + 5; //start timer  
                timeset = true;
            }
            if (currentTime < timeFinished) //if time is less than 5 do this->
            {
                mSpeed = 1;  
            } 
            else if(timeFinished > 5)
            {          
                mSpeed = 4;          
                feed = false; //break out of iteration
                timeset = false; //break out of iteration
                patrol = false; //break out of iteration
            }
             return tokenPosition;
        }
       #endregion

       #region pFeeding

        /* ----------------- FEEDING BEHAVIOUR --------------------  */
        private  Vector3 PFeeding(Vector3 tokenPosition)
        {

            /* --------- MOVE TOWARDS CHICKEN LIEG ----------------  */
            /* src: http://xnafan.net/2012/12/pointing-and-moving-towards-a-target-in-xna-2d/ */
            Vector3 getChickenLeg = tokenPosition - mAquarium.ChickenLeg.Position; // vector has position of chickenleg
            getChickenLeg.Normalize();  //set vector to 1

            tokenPosition = tokenPosition - getChickenLeg * mSpeed; //getting velocity / tokenPosition of Paranaha minus the position of chickenLeg

            if (tokenPosition.X <= mAquarium.ChickenLeg.Position.X) //less than chickenleg position->then
            {
                mFacingDirection = 1; //right
            }
            else
            {
                mFacingDirection = -1; //left
            }

            this.PossessedToken.Orientation = new Vector3(mFacingDirection,
                                               this.PossessedToken.Orientation.Y,
                                               this.PossessedToken.Orientation.Z);
            /* --------- DELETE CHICKEN LIEG ----------------  */
            /* src: http://forum.unity3d.com/threads/distance-between-two-objects.37918/ */
            if (Vector3.Distance(tokenPosition, mAquarium.ChickenLeg.Position) <= 5)
            {
                mAquarium.RemoveChickenLeg();
               
                feed = true; 
            }

           return tokenPosition;
        }

       #endregion

        public override void Update(ref GameTime pGameTime)
        {
            Vector3 tokenPosition = this.PossessedToken.Position;
            currentTime = pGameTime.TotalGameTime.TotalSeconds;
            if (!startingPos) //false
            {
                currentPosition = tokenPosition;
                startingPos = true; //break out of iteration
            }           

            this.PossessedToken.Orientation = new Vector3(mFacingDirection,
                                                this.PossessedToken.Orientation.Y,
                                                this.PossessedToken.Orientation.Z);
        

            if (mAquarium.ChickenLeg == null) //empty
            {
                tokenPosition = PHungry(tokenPosition); //passess object
                if (feed) //if true
                {
                    tokenPosition = fullState(tokenPosition); //speed of one for 5 secs
                }
            }
            else if (mAquarium.ChickenLeg != null) //not empty
            {
                tokenPosition = PFeeding(tokenPosition); //remove chickenLeg

                if(!patrol) //false
                {                    
                    patrol = true; //break out of iteration
                }

            } 
  
            this.PossessedToken.Position = tokenPosition; // possesses token *
        }
   
        #endregion
    }
}
