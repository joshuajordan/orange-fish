﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;              // Required to use XNA features.
using XNAMachinationisRatio;                // Required to use the XNA Machinationis Ratio Engine general features.
using XNAMachinationisRatio.AI;             // Required to use the XNA Machinationis Ratio general AI features.




namespace FishORama
{

    class SeaHorseMind : AIPlayer
    {
        #region Data Members

        private AquariumToken mAquarium;        // Reference to the aquarium in which the creature lives.

        /* ------------- INTS --------------------- */
        private int mFacingDirection = 1;         // Direction the fish is facing (1: right; -1: left).
        private int yDirection = 1;
        private int mSpeed;
        private int ySpeed;

        /* ------------- VECTORS --------------------- */
        private Vector3 tokenPosition;
        private Vector3 firstPos;

        /* ------------- BOOLEANS --------------------- */
        private bool Scatter;

        /* ------------- DOBULES --------------------- */
        double xDiff;
        double yDiff;
        double xyDirection;
        double xSpeed = 0;
        double speedVertical = 0;

        /* ------------- FLOATS --------------------- */
        float speedHorizontal;
        float speedUpwards;
        float distanceTraveled = 0;

        /* ------------- RANDOM CLASS --------------------- */
        private static Random random = new Random();

        #endregion

        #region Properties

        /// <summary>
        /// Set Aquarium in which the mind's behavior should be enacted.
        /// </summary>
        public AquariumToken Aquarium
        {
            set { mAquarium = value; }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="pToken">Token to be associated with the mind.</param>
        public SeaHorseMind(X2DToken pToken)
        {
            mFacingDirection = 1;
            this.Possess(pToken);
            Scatter = false;
            firstPos = new Vector3(pToken.Position.X, pToken.Position.Y, pToken.Position.Z); //initial position

            this.mSpeed = random.Next(5, 10);
            this.ySpeed = random.Next(1, 8);
        }

        #endregion

        #region Methods


        /// <summary>
        /// AI Update method.
        /// </summary>
        /// <param name="pGameTime">Game time</param>

        public Vector3 SwimBehvaiour(Vector3 tokenPosition)
        {        

            if (tokenPosition.X > 350 || tokenPosition.X < -350) 
            { 
                mFacingDirection *= -1;
              
            }

            if (tokenPosition.Y > firstPos.Y + 100 || tokenPosition.Y < firstPos.Y - 100) // + 100 and - 100 on y axes
            {               
                yDirection *= -1;
            }


            tokenPosition.X = tokenPosition.X + mSpeed * mFacingDirection; //x=direction
            tokenPosition.Y = tokenPosition.Y + ySpeed * yDirection; //=ydirection

            return tokenPosition;
        }

        public void ScatterBehaviour()
        {
            distanceTraveled = distanceTraveled += mSpeed;

            if(distanceTraveled < 100) //if less than 100
            {
            
            if (!Scatter) //false
            {
                /*to make the seahorses move away from the chickenleg I have  used trigonometry to find the angle of "rotation" between the chickenleg and each seahorse
                 and then calculate the x and y speed components*/

                yDiff = this.PossessedToken.Position.Y - mAquarium.ChickenLeg.Position.Y; //differnece in y coordinates betwwen seahorse and chickenLeg
                xDiff = this.PossessedToken.Position.X - mAquarium.ChickenLeg.Position.X; //differnece in x coordinates betwwen seahorse and chickenLeg
                xyDirection = Math.Atan2(yDiff, xDiff); //calculate the angle for the x and y components using the inverse tangent of yDifference / xDifference
                xSpeed = Math.Cos(xyDirection) * 10; // The cosine of our angle * the speed creates the x component
                speedVertical = Math.Sin(xyDirection) * 10; //The sine of the angle * the speed creates the y component
                
                speedHorizontal = (float)xSpeed; //Convert the xSpeed to a float
                speedUpwards = (float)speedVertical; //Convert the ySpeed to a float

                Scatter = true;  //Set scatter to true
               

                if (speedHorizontal >= 0 && xDiff >= 0) //if speed is greater than 0 -> then seahorse is to the right of chickenleg
                {
                    speedHorizontal *= -1;
                    mFacingDirection = -1;
                }

                if (speedUpwards >= 0 && yDiff <= 0) //if seahorse is moving upwards and is higher than the chickenleg
                {
                    speedUpwards *= -1;
                }

                this.tokenPosition.X = tokenPosition.X + speedHorizontal * mFacingDirection; //movement
                this.tokenPosition.Y = tokenPosition.Y + speedUpwards; //movement

 
                    if (tokenPosition.X > 350 || tokenPosition.X < 0) //edge of border
                    {
                        xSpeed *= -1;
                        mFacingDirection = 1;
                       Scatter = false; //break out of iteration
                    }

                    if (tokenPosition.Y > 250 || tokenPosition.Y < 0) //edge of border
                    {
                        ySpeed *= -1;
                        mFacingDirection = -1;
                       Scatter = false; //break out of iteration
                } 
              }
            }
            else
            {
                Scatter = false;
                distanceTraveled = 0; //reset
            }

        }


        public override void Update(ref GameTime pGameTime)
        {

            Vector3 tokenPosition = this.PossessedToken.Position;
            tokenPosition = SwimBehvaiour(tokenPosition);

      
            if (mAquarium.ChickenLeg != null)
            {
                ScatterBehaviour();
                Scatter = false; //reset iteration
            }


            this.PossessedToken.Position = tokenPosition;

            this.PossessedToken.Orientation = new Vector3(mFacingDirection,
                                           this.PossessedToken.Orientation.Y,
                                           this.PossessedToken.Orientation.Z);


        }

    }

        #endregion
}
