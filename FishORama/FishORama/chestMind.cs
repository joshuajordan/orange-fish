﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;              // Required to use XNA features.
using XNAMachinationisRatio;                // Required to use the XNA Machinationis Ratio Engine general features.
using XNAMachinationisRatio.AI;             // Required to use the XNA Machinationis Ratio general AI features.
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Input;


namespace FishORama
{

    class chestMind : AIPlayer
    {
        #region Data Members
        private AquariumToken mAquarium;    // Reference to the aquarium in which the creature lives.
        #endregion
        private float Rightspeed = 5;
        private float Leftspeed = -5;
        private bool Move = false; 

        #region Properties

        /// <summary>
        /// Set Aquarium in which the mind's behavior should be enacted.
        /// </summary>
        public AquariumToken Aquarium
        {
            set { mAquarium = value; }
        }

        /// <summary>
        /// Get/Set initial position on Y axis.
        /// </summary>


        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="pToken">Token to be associated with the mind.</param>
        public chestMind(X2DToken pToken)
        {
            this.Possess(pToken);       // Possess token.
        }

        #endregion

        #region Methods

        /// <summary>
        /// AI Update method.
        /// </summary>
        /// <param name="pGameTime">Game time</param>
        /// 
        /* --------------- RIGHT KEY Method ----------------*/
        private Vector3 Right(Vector3 tokenPosition)
        {
            tokenPosition.X = tokenPosition.X + Rightspeed; //move right -> right speed = 5
       
            return tokenPosition;
        }

        /* --------------- LEFT KEY METHOD ----------------*/
        private Vector3 Left(Vector3 tokenPosition)
        {
            tokenPosition.X = tokenPosition.X + Leftspeed; //move left -> left speed = -5;

            return tokenPosition;
        }


        public override void Update(ref GameTime pGameTime)
        {
            Vector3 tokenPosition = this.PossessedToken.Position;

            var keystate = Keyboard.GetState(); //keystate

            /* --------------- RIGHT KEY ----------------*/

            if (keystate.IsKeyDown(Keys.Right)) 
            {
                Console.WriteLine(Move);
                Move = true;
                if (Move == true)
                {
                   tokenPosition =  Right(tokenPosition);
                    Move = false;
                }
            }
           /* ----------------- END OF RIGHT KEY -------------*/


            /* --------------- LEFT KEY ----------------------*/
                if (keystate.IsKeyDown(Keys.Left))
                {

                    Console.WriteLine(Move);
                    Move = true;
                    if (Move == true)
                    {
                        tokenPosition =  Left(tokenPosition);
                        Move = false;
                    }
                }
           /* --------------- END OF LEFT KEY ----------------*/
               this.PossessedToken.Position = tokenPosition;

            }

        #endregion
        }


    }



