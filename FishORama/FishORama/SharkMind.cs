﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;              // Required to use XNA features.
using XNAMachinationisRatio;                // Required to use the XNA Machinationis Ratio Engine general features.
using XNAMachinationisRatio.AI;             // Required to use the XNA Machinationis Ratio general AI features.
using System.Diagnostics;
using System.Threading;

/* LERNING PILL: XNAMachinationisRatio Engine
 * XNAMachinationisRatio is an engine that allows implementing
 * simulations and games based on XNA, simplifying the use of XNA
 * and adding features not directly available in XNA.
 * XNAMachinationisRatio is a work in progress.
 * The engine works "under the hood", taking care of many features
 * of an interactive simulation automatically, thus minimizing
 * the amount of code that developers have to write.
 * 
 * In order to use the engine, the application main class (Kernel, in the
 * case of FishO'Rama) creates, initializes and stores
 * an instance of class Engine in one of its data members.
 * 
 * The classes comprised in the  XNA Machinationis Ratio engine and the
 * related functionalities can be accessed from any of your XNA project
 * source code files by adding appropriate 'using' statements at the beginning of
 * the file. 
 * 
 */

namespace FishORama
{
    /* LEARNING PILL: Token behaviors in the XNA Machinationis Ratio engine
     * Some simulation tokens may need to enact specific behaviors in order to
     * participate in the simulation. The XNA Machinationis Ratio engine
     * allows a token to enact a behavior by associating an artificial intelligence
     * mind to it. Mind objects are created from subclasses of the class AIPlayer
     * included in the engine. In order to associate a mind to a token, a new
     * mind object must be created, passing to the constructor of the mind a reference
     * of the object that must be associated with the mind. This must be done in
     * the DefaultProperties method of the token.
     * 
     * Hence, every time a new tipe of AI mind is required, a new class derived from
     * AIPlayer must be created, and an instance of it must be associated to the
     * token classes that need it.
     * 
     * Mind objects enact behaviors through the method Update (see below for further details). 
     */
    class SharkMind : AIPlayer
    {

        #region Data Members

        // This mind needs to interact with the token which it possesses, 
        // since it needs to know where are the aquarium's boundaries.
        // Hence, the mind needs a "link" to the aquarium, which is why it stores in
        // an instance variable a reference to its aquarium.
        private AquariumToken mAquarium;        // Reference to the aquarium in which the creature lives.

        /* ------------- BOOLEANS --------------- */
        private bool timeSet = false;
        private bool doBehaviour = true;
        private bool pickBehaviour = false;
        private bool feed = false;

        /* ------------- FLOATS --------------- */
        private float sinking;
        private float origionalPosition;
        private float sinkDistance;
        private float distanceTraveled = 0;
        private float mFacingDirection;         // Direction the fish is facing (1: right; -1: left).
        private float mSpeed = 3;
        private float yFacingDirection;

        /* ------------- DOBULES --------------- */
        private double currentTime;
        private double timeFinished;
     

        /* ------------- INTS --------------- */
        private int accelTime = 900;
        private int pick;

        /* ------------- RANDOM CLASS --------------- */

        Random random = new Random();
        private Random randomBehaviourGenerator = new Random();

        #endregion

        #region Properties

        /// <summary>
        /// Set Aquarium in which the mind's behavior should be enacted.
        /// </summary>
        public AquariumToken Aquarium
        {
            set { mAquarium = value; }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="pToken">Token to be associated with the mind.</param>
        public SharkMind(X2DToken pToken)
        {
            /* LEARNING PILL: associating a mind with a token
             * In order for a mind to control a token, it must be associated with the token.
             * This is done when the mind is constructed, using the method Possess inherited
             * from class AIPlayer.
             */
            mFacingDirection = 1;
            this.Possess(pToken);
        }

        #endregion

        #region Methods


        /// <summary>
        /// AI Update method.
        /// </summary>
        /// <param name="pGameTime">Game time</param>
        #region pHungry
        private Vector3 PHungry(Vector3 tokenPosition)
        {
            tokenPosition.X = tokenPosition.X + mSpeed * mFacingDirection;

            if (tokenPosition.X > 350 || tokenPosition.X < -350)
            {
                mFacingDirection = -mFacingDirection;
                mSpeed = 3;
            }

            return tokenPosition;
        }
        #endregion


        #region pFeeding
        private Vector3 PFeeding(Vector3 tokenPosition)
        {

            /* --------- MOVE TOWARDS CHICKEN LIEG ----------------  */

            /* src: http://xnafan.net/2012/12/pointing-and-moving-towards-a-target-in-xna-2d/ */
            Vector3 getMarker1 = tokenPosition - mAquarium.Marker1.Position; // vector has position of chickenleg
            
           getMarker1.Normalize();  //set vector to 1


            tokenPosition = tokenPosition - getMarker1 * mSpeed; //getting velocity / tokenPosition of shark minus the position of chickenLeg

            if (tokenPosition.X <= mAquarium.Marker1.Position.X) //less than position->then
            {
                mFacingDirection = 1; //right
            }
            else
            {
                mFacingDirection = -1; //left
            }

            this.PossessedToken.Orientation = new Vector3(mFacingDirection,
                                               this.PossessedToken.Orientation.Y,
                                               this.PossessedToken.Orientation.Z);


            /* --------- DELETE CHICKEN LIEG ----------------  */
            /* src: http://forum.unity3d.com/threads/distance-between-two-objects.37918/ */
            if (Vector3.Distance(tokenPosition, mAquarium.Marker1.Position) <= 5) //checks both and if positions are less than 5 ->then -> removemarker()
            {
                mAquarium.RemoveMarker1();
                pickBehaviour = false; //restart randomNumber generator
                feed = true; 
            }
            return tokenPosition;
        }


        /* -------------- Behaviours ---------------*/

        private Vector3 VerticalSwimBehaviour(Vector3 tokenPosition)
        {
            tokenPosition.Y = tokenPosition.Y + mSpeed * yFacingDirection;

            this.PossessedToken.Position = tokenPosition;

            if (tokenPosition.Y > 300 || tokenPosition.Y < -300) // vDistance = verticalDistance
            {
                // yFacingDirection = -yFacingDirection;
            }

            return tokenPosition;
        }

        /* ---------------  HUNGRY BEHAVIOUR -----------------  */
        private Vector3 HungryBehvaiour(Vector3 tokenPosition)
        {
            if (doBehaviour) //if true
            {
                distanceTraveled = 0; //reset
                mSpeed = 1;
                accelTime = 300;
                doBehaviour = false; //break out of iteration
            }

            if (distanceTraveled >= 150) //swim back forth 150px
            {
                distanceTraveled = 0; //turn around and count again
                mFacingDirection = -mFacingDirection;
            }

            if (tokenPosition.Y <= 250) //less than-> then
            {
                yFacingDirection = 1; //right
                tokenPosition = VerticalSwimBehaviour(tokenPosition); //swimupwards
            }
            accelTime -= 1; //decrease
            if (accelTime == 1) //if timer is greater than 5
            {
                distanceTraveled = 0; //turn around and count again
                doBehaviour = true; //break out of iteration 
                pickBehaviour = false; //break out of case iteration->restart number generator
            }

            return tokenPosition;

        }

        /* ---------------  SINK BEHAVIOUR -----------------  */
        private Vector3 SinkBehaviour(Vector3 tokenPosition)
        {
            if (doBehaviour) //if ture->do this
            {
                sinking = randomBehaviourGenerator.Next(50, 150); //sink between 50 - 150

                origionalPosition = tokenPosition.Y;
                sinkDistance = origionalPosition - sinking;
                yFacingDirection = -1;

                distanceTraveled = 0; //reset
                doBehaviour = false; //break out of iteration
            }

            if (sinkDistance < -200) //distance travled less than -> then
            {
                sinkDistance = -200; //down by value set
            }

            if (tokenPosition.Y > sinkDistance)
            {
                yFacingDirection = -1;//force down
                tokenPosition = VerticalSwimBehaviour(tokenPosition);

            }
            else
            {
                doBehaviour = true; //break out of iteration
                pickBehaviour = false; //break out of case iteration
            }
            return tokenPosition;
        }

        /* -------------- END - of swimming Behaviours---------------- */

        #endregion

        public override void Update(ref GameTime pGameTime)
        {

            currentTime += pGameTime.ElapsedGameTime.TotalSeconds;

            Vector3 tokenPosition = this.PossessedToken.Position;

            if (mAquarium.Marker1 == null) //empty
            {
                tokenPosition = PHungry(tokenPosition); //passess object
                timeSet = false; //restart timer iteration
                if (feed) //if true
                {
                   tokenPosition = HungryBehvaiour(tokenPosition); //hungry->wants more food
                }
            }
            else if (mAquarium.Marker1 != null) //not empty
            {
                if (!timeSet) //flase
                {
                    timeFinished = currentTime + 5; //wait 5 seconds to eat the marker
                    timeSet = true; //break out of iteration
                    pickBehaviour = true; //stop timer for switchcase
                }
                if (timeFinished < currentTime && timeSet) //becomes less than current time and timeset == false
                {
                    tokenPosition = PFeeding(tokenPosition); //remove marker
                }
                        
            }
         
            if (!pickBehaviour) //if not true
            {
                pick = randomBehaviourGenerator.Next(1, 250);
            }

            switch (pick) //if true
            {
                case 1:
                    {
                        pickBehaviour = true; //break out of case iteration
                        tokenPosition = HungryBehvaiour(tokenPosition);
                    }
                    break;
                case 2:
                    {
                        pickBehaviour = true;
                        tokenPosition = SinkBehaviour(tokenPosition);
                    }
                    break;           

                default:
                    tokenPosition = PHungry(tokenPosition);
                    break;
            }

            this.PossessedToken.Orientation = new Vector3(mFacingDirection,
                                                this.PossessedToken.Orientation.Y,
                                                this.PossessedToken.Orientation.Z);

            this.PossessedToken.Position = tokenPosition; // possesses token *

        }

        #endregion
    }
}
